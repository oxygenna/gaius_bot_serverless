'use strict';
require('dotenv').load();
//Twilio Bot
const Twilio = require('twilio');
const AccessToken = Twilio.jwt.AccessToken;
const ChatGrant = AccessToken.ChatGrant;
const studioFlowSid = 'FW9c6523b26ab2296c017a81f2d53bc153';
const client = require('twilio')(process.env.TWILIO_ACCOUNT_SID, 'c9376bf6be3d930f23f12c9bdb1f8dea');
const VoiceResponse = Twilio.twiml.VoiceResponse;
//Google Vision
const vision = require('@google-cloud/vision');

//Google Bucket
const { Storage, File } = require('@google-cloud/storage');
const storage = new Storage();

const fetch = require('node-fetch');

// NeverBounce
const NeverBounce = require('neverbounce');
const clientneverbounce = new NeverBounce({ apiKey: 'secret_06ce173392f0e8363661c8ca0fba4319' });

//FireBase
const admin = require('firebase-admin');
const serviceAccount = require('./firebaseCreds/gaius-sms-chat-firebase-adminsdk-akm3w-d5c0211774');
admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
});
const db = admin.firestore();

exports.callback = async (request, response) => {
  if (request.method === 'POST') {
    var phoneNumber = request.body.userPhone;
    var toCall = '+491637726363';

    var url = 'http://' + request.headers.host + '/g-serverless-dev-outbound/' + toCall;

    var options = {
      to: phoneNumber,
      from: '+4915735998080',
      url: url,
    };

    client.calls
      .create(options)
      .then((message) => {
        console.log(message.responseText);
        response
          .set('Access-Control-Allow-Origin', '*')
          .set('Access-Control-Allow-Headers', '*')
          .set('Access-Control-Allow-Methods', 'POST')
          .status(200)
          .send('success');
      })
      .catch((error) => {
        console.log(error);
        response.status(500).send(error);
      });
  }
  if (request.method === 'OPTIONS') {
    response
      .set('Access-Control-Allow-Origin', '*')
      .set('Access-Control-Allow-Headers', '*')
      .set('Access-Control-Allow-Methods', 'POST')
      .status(204)
      .send('');
  }
};

exports.outbound = async (request, response) => {
  if (request.method === 'POST') {
    var twimlResponse = new VoiceResponse();

    twimlResponse.say('Thank you for calling' + 'you will be connected with an agent sortly', {
      voice: 'alice',
    });

    twimlResponse.dial(request.params['0']);

    response.send(twimlResponse.toString());
  }
  if (request.method === 'OPTIONS') {
    response
      .set('Access-Control-Allow-Origin', '*')
      .set('Access-Control-Allow-Headers', '*')
      .set('Access-Control-Allow-Methods', 'POST')
      .status(204)
      .send('');
  }
};

exports.OCRgoogle = async (req, response) => {
  console.log('inside OCR');
  if (req.method === 'POST') {
    const clientOptions = { apiEndpoint: 'eu-vision.googleapis.com' };
    const clientOcr = new vision.ImageAnnotatorClient(clientOptions);
    const bucketName = 'fax_bucket';
    const fileNam = req.body;
    const fileName = fileNam + '.pdf';
    const outputFile = fileNam;
    const outputPrefix = 'results';
    console.log('fileNam', fileNam);
    const bucketFile = storage.bucket('fax_bucket').file(fileName);

    const gcsSourceUri = `gs://${bucketName}/${fileName}`;
    const gcsDestinationUri = `gs://${bucketName}/${outputPrefix}/${outputFile}`;

    const inputConfig = {
      mimeType: 'application/pdf',
      gcsSource: {
        uri: gcsSourceUri,
      },
    };
    const outputConfig = {
      gcsDestination: {
        uri: gcsDestinationUri,
      },
    };
    const features = [{ type: 'DOCUMENT_TEXT_DETECTION' }];
    const request = {
      requests: [
        {
          inputConfig: inputConfig,
          features: features,
          outputConfig: outputConfig,
        },
      ],
    };
    try {
      const [operation] = await clientOcr.asyncBatchAnnotateFiles(request);
      const [filesResponse] = await operation.promise();
      const destinationUri = filesResponse.responses[0].outputConfig.gcsDestination.uri;
      console.log('Json saved to: ' + destinationUri);
      fetch('https://europe-west1-gaius-chatbot-serverless.cloudfunctions.net/uploadBucket', {
        method: 'POST',
        body: fileNam,
      });
    } catch (err) {
      console.error('ERROR:', err);
    }
    response.status(200).send('success');
  }
  if (req.method === 'OPTIONS') {
    response
      .set('Access-Control-Allow-Origin', '*')
      .set('Access-Control-Allow-Headers', '*')
      .set('Access-Control-Allow-Methods', 'POST')
      .status(204)
      .send('');
  }
};

exports.uploadBucket = async (req, res) => {
  if (req.method === 'POST') {
    function Compare(strA, strB) {
      for (var result = 0, i = strA.length; i--; ) {
        if (typeof strB[i] == 'undefined' || strA[i] == strB[i]);
        else if (strA[i].toLowerCase() == strB[i].toLowerCase()) result++;
        else result += 4;
      }
      return 1 - (result + 4 * Math.abs(strA.length - strB.length)) / (2 * (strA.length + strB.length));
    }

    const fileNam = req.body;
    const fileName = req.body + '.pdf';
    console.log('fileName', fileName);
    const jsonFilePath = 'results/' + fileNam + 'output-1-to-1.json';
    var archivo = storage.bucket('fax_bucket').file(jsonFilePath).createReadStream();
    console.log('Concat Data');
    var buf = '';
    archivo
      .on('data', function (d) {
        buf += d;
      })
      .on('end', async function () {
        var responseToObject = JSON.parse(buf);

        var extractTextFromJson = responseToObject.responses[0].fullTextAnnotation.text;
        console.log('extractTextFromJson', extractTextFromJson);

        var mySubStringGid = extractTextFromJson.substring(extractTextFromJson.lastIndexOf('CID'));

        var GID = mySubStringGid.substring(4, 14).trim();
        GID = GID.substring(0, 8);

        const [files] = await storage.bucket('fax_bucket').getFiles({ prefix: 'CID_' });
        console.log('Files:');
        //Similarity Procedures
        var incomingCID = GID;
        var CIDS = [];
        files.forEach((file) => {
          var fileCID = file.name.replace('CID_', '');
          fileCID = fileCID.substring(0, 8);
          if (!CIDS.includes(fileCID)) {
            CIDS.push(fileCID);
          }
        });

        var CIDSimilarityThreshold = [];
        for (var i = 0; i < CIDS.length; i++) {
          var similarityCheck = Compare(CIDS[i], incomingCID);
          var similarityAbsoluteMatch = false;
          console.log('Similarity', similarityCheck);
          if (similarityCheck == 1) {
            console.log('inside', similarityCheck, CIDS[i]);
            similarityAbsoluteMatch = true;
            CIDSimilarityThreshold.push(CIDS[i]);
          } else if (similarityCheck > 0.8) {
            console.log('inside', similarityCheck, CIDS[i]);
            CIDSimilarityThreshold.push(CIDS[i]);
          }
        }
        console.log('similarityAbsoluteMatch', similarityAbsoluteMatch);
        console.log('CIDSimilarityThreshold', CIDSimilarityThreshold.length);
        if (similarityAbsoluteMatch) {
          console.log('Absolute match');
          const file = storage.bucket('fax_bucket').file(fileName);
          const movePath = 'CID_' + GID + '/' + fileName;
          console.log('movePath', movePath);
          file.move(movePath, function (err, destinationFile, apiResponse) {});
        } else if (CIDSimilarityThreshold.length > 1) {
          console.log('greater');
          const file = storage.bucket('fax_bucket').file(fileName);
          const movePath = 'DIDNOTMATCHANY/' + GID + '.pdf';
          console.log('movePath', movePath);
          file.move(movePath, function (err, destinationFile, apiResponse) {});
        } else if (CIDSimilarityThreshold.length == 0) {
          const file = storage.bucket('fax_bucket').file(fileName);
          const movePath = 'DIDNOTMATCHANY/GIDnotFound' + fileName;
          console.log('movePath', movePath);
          file.move(movePath, function (err, destinationFile, apiResponse) {});
        } else {
          console.log('execute code', CIDSimilarityThreshold);
          const file = storage.bucket('fax_bucket').file(fileName);
          const movePath = 'CID_' + CIDSimilarityThreshold + '/' + fileName;
          console.log('movePath', movePath);
          file.move(movePath, function (err, destinationFile, apiResponse) {});
        }
        res.send(buf);
      });
  }
};

exports.sendoutboundfax = (request, response) => {
  if (request.method === 'POST') {
    console.log('req', request.body.MediaURL);
    client.fax.faxes
      .create({
        from: '+4915735998080',
        to: '+4930255585276',
        mediaUrl: request.body.MediaURL,
      })
      .then((fax) => {
        response
          .set('Access-Control-Allow-Origin', '*')
          .set('Access-Control-Allow-Headers', '*')
          .set('Access-Control-Allow-Methods', 'POST')
          .status(200)
          .send(fax.sid);
      });
  }
  if (request.method === 'OPTIONS') {
    response
      .set('Access-Control-Allow-Origin', '*')
      .set('Access-Control-Allow-Headers', '*')
      .set('Access-Control-Allow-Methods', 'POST')
      .status(204)
      .send('');
  }
};

exports.sendfax = (request, response) => {
  console.log('sendFax');
  if (request.method === 'POST') {
    const twiml = `
  <Response>
    <Receive action="https://europe-west1-gaius-chatbot-serverless.cloudfunctions.net/faxreceived"/>
  </Response>
  `;
    response.send(twiml);
  }
  if (request.method === 'OPTIONS') {
    response
      .set('Access-Control-Allow-Origin', '*')
      .set('Access-Control-Allow-Headers', '*')
      .set('Access-Control-Allow-Methods', 'POST')
      .status(204)
      .send('');
  }
};

exports.faxreceived = (request, response) => {
  if (request.method === 'POST') {
    console.log('faxreceived');

    const bucket = storage.bucket('fax_bucket');
    const url = request.body.MediaUrl;

    const fileName = Math.random().toString(36).substring(2);
    const file = bucket.file(fileName + '.pdf');

    const writeStream = file.createWriteStream();

    fetch(url).then((res) => {
      res.body.pipe(writeStream);
    });

    writeStream.on('finish', () => {
      setTimeout(function () {
        fetch('https://europe-west1-gaius-chatbot-serverless.cloudfunctions.net/OCRgoogle', {
          method: 'POST',
          body: fileName,
        });
      }, 5000);
    });

    response.send('success');
  }
  if (request.method === 'OPTIONS') {
    response
      .set('Access-Control-Allow-Origin', '*')
      .set('Access-Control-Allow-Headers', '*')
      .set('Access-Control-Allow-Methods', 'POST')
      .status(204)
      .send('');
  }
};

exports.createtoken = (request, response) => {
  if (request.method === 'POST') {
    const tokenID = request.body.tokenID;
    const token = new AccessToken(
      process.env.TWILIO_ACCOUNT_SID,
      process.env.TWILIO_API_KEY,
      process.env.TWILIO_API_SECRET
    );

    token.identity = tokenID;
    token.addGrant(
      new ChatGrant({
        serviceSid: process.env.TWILIO_CHAT_SERVICE_SID,
      })
    );

    response
      .set('Access-Control-Allow-Origin', '*')
      .set('Access-Control-Allow-Headers', '*')
      .set('Access-Control-Allow-Methods', 'POST')
      .status(200)
      .send({
        identity: token.identity,
        jwt: token.toJwt(),
      });
  }
  if (request.method === 'OPTIONS') {
    response
      .set('Access-Control-Allow-Origin', '*')
      .set('Access-Control-Allow-Headers', '*')
      .set('Access-Control-Allow-Methods', 'POST')
      .status(204)
      .send('');
  }
};

exports.addbot = (request, response) => {
  if (request.method === 'POST') {
    const channelSid = request.body.channelSid;
    client.chat.services(process.env.TWILIO_CHAT_SERVICE_SID).channels(channelSid).webhooks.create({
      'configuration.flowSid': studioFlowSid,
      type: 'studio',
    });

    response
      .set('Access-Control-Allow-Origin', '*')
      .set('Access-Control-Allow-Headers', '*')
      .set('Access-Control-Allow-Methods', 'POST')
      .status(200)
      .send('success');
  }
  if (request.method === 'OPTIONS') {
    response
      .set('Access-Control-Allow-Origin', '*')
      .set('Access-Control-Allow-Headers', '*')
      .set('Access-Control-Allow-Methods', 'POST')
      .status(204)
      .send('');
  }
};

exports.deletechannel = (request, response) => {
  const channelSid = request.body.channelSid;
  client.chat.services(process.env.TWILIO_CHAT_SERVICE_SID).channels(channelSid).remove();

  response
    .set('Access-Control-Allow-Origin', '*')
    .set('Access-Control-Allow-Headers', '*')
    .set('Access-Control-Allow-Methods', 'POST')
    .status(200)
    .send('');
};

exports.getmobilenumbers = (request, response) => {
  if (request.method === 'POST') {
    client.messages
      .list({
        from: '+4915735998080',
      })
      .then((messages) => {
        const mobile_numbers = [...new Set(messages.map((q) => q.to))];
        response.set('Access-Control-Allow-Origin', '*').status(200).send({
          mobilenumbers: mobile_numbers,
        });
      });
  }
  if (request.method === 'OPTIONS') {
    response
      .set('Access-Control-Allow-Origin', '*')
      .set('Access-Control-Allow-Headers', '*')
      .set('Access-Control-Allow-Methods', 'POST')
      .status(204)
      .send('');
  }
};

exports.getsms = (request, response) => {
  if (request.method === 'POST') {
    client.messages
      .list({
        from: '+4915735998080',
        limit: 2000,
      })
      .then((messages) => {
        response.set('Access-Control-Allow-Origin', '*').status(200).send({
          messages,
        });
      });
  }
  if (request.method === 'OPTIONS') {
    response
      .set('Access-Control-Allow-Origin', '*')
      .set('Access-Control-Allow-Headers', '*')
      .set('Access-Control-Allow-Methods', 'POST')
      .status(204)
      .send('');
  }
};

exports.sendSms = (request, response) => {
  if (request.method === 'POST') {
    const text = request.body.text;
    const number = request.body.mobile_number;
    client.messages.create({ body: text, from: '+4915735998080', to: number });
    response
      .set('Access-Control-Allow-Origin', '*')
      .set('Access-Control-Allow-Headers', '*')
      .set('Access-Control-Allow-Methods', 'POST')
      .status(200)
      .send('success');
  }
  if (request.method === 'OPTIONS') {
    response
      .set('Access-Control-Allow-Origin', '*')
      .set('Access-Control-Allow-Headers', '*')
      .set('Access-Control-Allow-Methods', 'POST')
      .status(204)
      .send('');
  }
};

exports.getallsms = (request, response) => {
  if (request.method === 'POST') {
    client.messages
      .list({
        from: request.body.mobile_number,
        to: '+4915735998080',
      })
      .then((messages) => {
        response
          .set('Access-Control-Allow-Origin', '*')
          .set('Access-Control-Allow-Headers', '*')
          .set('Access-Control-Allow-Methods', 'POST')
          .status(200)
          .send(messages);
      });
  }
  if (request.method === 'OPTIONS') {
    response
      .set('Access-Control-Allow-Origin', '*')
      .set('Access-Control-Allow-Headers', '*')
      .set('Access-Control-Allow-Methods', 'POST')
      .status(204)
      .send('');
  }
};

exports.mobileLookUp = (request, response) => {
  if (request.method === 'POST') {
    const number = request.body.mobile_number;
    client.lookups
      .phoneNumbers(number)
      .fetch()
      .then((validresponse) => {
        response
          .set('Access-Control-Allow-Origin', '*')
          .set('Access-Control-Allow-Headers', '*')
          .set('Access-Control-Allow-Methods', 'POST')
          .status(200)
          .send(validresponse);
      });
  }
  if (request.method === 'OPTIONS') {
    response
      .set('Access-Control-Allow-Origin', '*')
      .set('Access-Control-Allow-Headers', '*')
      .set('Access-Control-Allow-Methods', 'POST')
      .status(204)
      .send('');
  }
};

exports.neverbounce = (request, response) => {
  const email = request.body.email;
  if (request.method === 'POST') {
    clientneverbounce.single.check(email).then((result) => {
      response
        .set('Access-Control-Allow-Origin', '*')
        .set('Access-Control-Allow-Headers', '*')
        .set('Access-Control-Allow-Methods', 'POST')
        .status(200)
        .send(result.getResult());
    });
  }
  if (request.method === 'OPTIONS') {
    response
      .set('Access-Control-Allow-Origin', '*')
      .set('Access-Control-Allow-Headers', '*')
      .set('Access-Control-Allow-Methods', 'POST')
      .status(204)
      .send('');
  }
};

exports.mobileNumbers = (request, response) => {
  db.collection('SMSmessages')
    .get()
    .then((snapshot) => {
      let existingNumbers = [];
      snapshot.forEach((doc) => {
        existingNumbers.push(doc.id);
      });
      response
        .set('Access-Control-Allow-Origin', '*')
        .set('Access-Control-Allow-Headers', '*')
        .set('Access-Control-Allow-Methods', 'POST')
        .status(200)
        .send(existingNumbers);
    })
    .catch((err) => {
      response
        .set('Access-Control-Allow-Origin', '*')
        .set('Access-Control-Allow-Headers', '*')
        .set('Access-Control-Allow-Methods', 'POST')
        .status(200)
        .send(err);
    });
};

exports.mobilevalidator = (request, response) => {
  const number = request.body.mobile_number;
  if (request.method === 'POST') {
    client.lookups
      .phoneNumbers(number)
      .fetch()
      .then((phone_number) => {
        response
          .set('Access-Control-Allow-Origin', '*')
          .set('Access-Control-Allow-Headers', '*')
          .set('Access-Control-Allow-Methods', 'POST')
          .status(200)
          .send(phone_number);
      });
  }
  if (request.method === 'OPTIONS') {
    response
      .set('Access-Control-Allow-Origin', '*')
      .set('Access-Control-Allow-Headers', '*')
      .set('Access-Control-Allow-Methods', 'POST')
      .status(204)
      .send('');
  }
};
